//
//  main.cpp
//  OpenGL Advances Lighting
//
//  Created by CGIS on 28/11/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#define TINYOBJLOADER_IMPLEMENTATION

#include "Model3D.hpp"
#include "Mesh.hpp"

//Screen parameters
int glWindowWidth = 1920;
int glWindowHeight = 1080;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

//Projection and Light
glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;
glm::vec3 lightPosEye;
GLuint lightPosEyeLoc;
glm::vec3 dirLightPos;
GLuint dirLightPosLoc;
glm::vec2 lightIntensity;
GLuint lightIntensityLoc;

//Camera
gps::Camera myCamera(glm::vec3(0.0f, 0.0f, 2.5f), glm::vec3(0.0f, 0.0f, -10.0f));
float cameraSpeed = 5.0f;

bool pressedKeys[1024];
float angle = 0.0f;

//for mouse callback
GLfloat prevX = 0;
GLfloat prevY = 0;

//objects
// I made a whole scene in 3ds max
gps::Model3D sceneModel;
gps::Model3D sanieModel;
//shader
gps::Shader myCustomShader;
gps::Shader skyboxShader;

//skybox
gps::SkyBox mySkyBox;
std::vector<const GLchar*> faces;

//movement and animation variables
float intensity = 4.0f;
int alreadyReset = 0;
bool resetCamera = false;
bool mouseMoved = false;

float lightx = -8.799998f;
float lighty = 4.499998f;
float lightz = -76.699402f;

float saniex = 0;
float saniey = 30.0f;
float saniez = -100.0f;
float PI = 3.14152f;

bool turnLeft = false;
bool turnRight = false;
bool go = false;
bool stanga = false;
float rotateAngle = 0.0f;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 10000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	float xOffset;
	float yOffset;

	if (!mouseMoved)
	{
		prevX = xpos;
		prevY = ypos;
		mouseMoved = true;
	}

	xOffset = xpos - prevX;
	yOffset = prevY - ypos;

	xOffset *= 0.1f;
	yOffset *= 0.1f;

	prevX = xpos;
	prevY = ypos;

	myCamera.rotate(yOffset, xOffset);
}

void moveCamera()
{
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_R])
	{
		resetCamera = true;
	}

	if (pressedKeys[GLFW_KEY_Y]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (pressedKeys[GLFW_KEY_U]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
}

void moveLight()
{
	if (pressedKeys[GLFW_KEY_UP])
	{
		lighty += 0.1f;
	}

	if (pressedKeys[GLFW_KEY_DOWN])
	{
		lighty -= 0.1f;
	}

	if (pressedKeys[GLFW_KEY_LEFT])
	{
		lightx -= 0.1f;
	}

	if (pressedKeys[GLFW_KEY_RIGHT])
	{

		lightx += 0.1f;
	}

	

	if (pressedKeys[GLFW_KEY_SPACE])
	{
		lightz -= 0.1f;
	}

	if (pressedKeys[GLFW_KEY_B])
	{
		lightz += 0.1f;
	}
	
	if (pressedKeys[GLFW_KEY_I])
	{
		intensity += 1;
	}

	if (pressedKeys[GLFW_KEY_O])
	{
		intensity -= 1;
		if (intensity <= 4) {
			intensity = 4;
		}
	}

	if (pressedKeys[GLFW_KEY_G]) {
		go = true;
	}

	if (pressedKeys[GLFW_KEY_H]) {
		go = false;
	}

}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//MSAA - Multi-Sample Anti-Aliasing
	glfwWindowHint(GLFW_SAMPLES, 16);
	glEnable(GL_MULTISAMPLE);

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "Winter Scene", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	glClearColor(0.3, 0.3, 0.3, 1.0);
	glViewport(0, 0, retina_width, retina_height);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise

	//altfel erau texturile foare intunecate
	glEnable(GL_FRAMEBUFFER_SRGB);
}

void initModels()
{
	sceneModel = gps::Model3D("objects/scene/sceneObject.obj", "objects/scene/");
	sanieModel = gps::Model3D("objects/sanie/sanieObject.obj", "objects/sanie/");
}

void initShaders()
{
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	myCustomShader.useShaderProgram();
}

void initUniforms()
{
	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	
	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 100000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 0.0f, 1.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 0.7f, 0.9f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	//set Dominant Directional Light position
	dirLightPos = glm::vec3(-1, -1, 0);
	dirLightPosLoc = glGetUniformLocation(myCustomShader.shaderProgram, "dirLightPos");
	glUniform3fv(dirLightPosLoc, 1, glm::value_ptr(dirLightPos));
}

void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	moveCamera();
	moveLight();

	if (resetCamera == true)
	{
		myCamera.reset(glm::vec3(0, 5.0f, 10.0f),
					   glm::vec3(10.0f,8.0f, -50.0f));
		resetCamera = false;
		saniex = 0;
		saniey = 30.0f;
		saniez = -100.0f;
	}

	//initialize the view matrix
	view = myCamera.getViewMatrix();
	//send view matrix data to shader	
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	//create normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	

	//Scene
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, -10.0f, -100.0f));

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	sceneModel.Draw(myCustomShader);

	//Sanie
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(saniex, saniey, saniez));
	
	if (go) {
		if (saniex >= 600) {

			turnLeft = true;
			stanga = true;
		}
		if (saniex <= -600) {
			turnRight = true;
			stanga = false;
		}

		if (turnLeft) {
			rotateAngle = PI;
			turnLeft = false;
		}

		if (turnRight) {
			rotateAngle = 0;
			turnRight = false;
		}

		

		saniey += 0.3f;
		if (stanga) {

			saniex -= 0.4f;
		}
		else {
			saniex += 0.4f;
		}
		saniez -= 0.2f;
		
	}
	model = glm::rotate(model, rotateAngle, glm::vec3(0, 1, 0));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	sanieModel.Draw(myCustomShader);

	//Snowman Nose Light
	//set Point Light position
	lightPosEye = glm::vec3(lightx, lighty, lightz);
	lightPosEyeLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightPosEye");
	glUniform3fv(lightPosEyeLoc, 1, glm::value_ptr(lightPosEye));
	//set Point Light intensity
	lightIntensity = glm::vec2(intensity, 0);
	lightIntensityLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightIntensity");
	glUniform2fv(lightIntensityLoc, 1, glm::value_ptr(lightIntensity));

	//printf("x:%f  y:%f  z:%f ", lightx, lighty, lightz);
	//printf(" intensity:%f ", intensity);
	
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();

	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	mySkyBox.Draw(skyboxShader, view, projection);

	myCustomShader.useShaderProgram();

}

void loadSkyboxFaces()
{
	
	faces.push_back("objects/skybox/background/lightblue/right.png");
	faces.push_back("objects/skybox/background/lightblue/left.png");
	faces.push_back("objects/skybox/background/lightblue/top.png");
	faces.push_back("objects/skybox/background/lightblue/bottom.png");
	faces.push_back("objects/skybox/background/lightblue/back.png");
	faces.push_back("objects/skybox/background/lightblue/front.png");

	mySkyBox.Load(faces);

}

int main(int argc, const char * argv[]) {
	
	initOpenGLWindow();
	initOpenGLState();
	initModels();
	initShaders();
	initUniforms();	
	loadSkyboxFaces();

	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}

